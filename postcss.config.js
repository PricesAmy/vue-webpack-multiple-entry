module.exports = {
  plugins: [
    //自动添加css前缀
    require('autoprefixer'),
    // 移动端px自动转换rem
    require('postcss-plugin-px2rem')({ remUnit: 75, baseDpr: 2 })
  ]
}
